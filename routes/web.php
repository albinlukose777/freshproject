<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    //return "Hello world";
   // return view('welcome');
    return view('newWelcome');
    //$name=request('name');
  //  return view('test',['name'=>$name]);//lp passing value from the request string to view


   // return ['foo'=>'bar'];
});
/*
Route::get('/posts/{peewee}', function ($post) {
    $posts=[
        'my_first_post'=>'this is my first blog post',
        'my_second_post'=>'My second blog post'
    ];
    //post value not found
    if(!array_key_exists($post,$posts))
    {
        abort(404,'The post is not a valid one');
    }

   // return view(post,['post'=>[$posts[$post]]]);
    return view('post',['post'=>$posts[$post]]);//lp added for the unindexed aray value
});
*/
Route::get('/posts/{peewee}','PostControllers@show');
    Route::get('/welcome', function () {
    return view('welcome');
});

Route::get('/test', function () {
    return view('test');
});

Route::get('/contact', function () {
    return view('contact');

});

Route::get('/welcome', function () {
    return view('newWelcome');

});

Route::get('/about', function () {
   // $articles=App\Article::latest()->get();//order by decending order
    //return $article;
    return view('about',['articles'=>App\Article::latest()->get()]);

});

Route::get('/articles/create', 'ArticlesController@create');
Route::post('/articles/', 'ArticlesController@store');
Route::get('/articles/{article}', 'ArticlesController@show')->name('articles.show');
Route::get('/articles/{article}/edit', 'ArticlesController@edit');
Route::put('/articles/{article}', 'ArticlesController@update');


//Route::get('/articles', function () {
    //return view('article',['articles'=>App\Article::latest()->get()]);
    //lp using index method to load all the articles
Route::get('/articles/', 'ArticlesController@index')->name('articles.index');

Route::get('/projects/', 'ProjectController@create');
Route::post('/projects/', 'ProjectController@store');



//GET/articles-- show all the articles
//GET/articles/2-- show a specific article
//GET/articles/create-- shows a form to create a new article
//POST/articles--submit the new article--create anew article
//GET/articles/2/edit- open a form to edit the article with id 2
//PUT/articles/2/-- submit the edited view  back
//Delete/articles/2-- delete the view




//GET/articles/
