@extends ('newSimpleLayout')
@section('bulma')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">
<script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
@endsection

@section('content')
<div id="app" class="container">


    <form method="POST" action="/projects" @submit.prevent="onSubmit">
        @csrf
    <div class="field">
        <label class="label" for="title">Title</label>
        <div class="control">
            <input class="input @error('title') is-danger @enderror"
                   type="text"
                   placeholder="Project title"
                   name ="title"
                   id="title"
                   value="{{old('title')}}"
                   v-model="title"
            >
            @error('title')
            <p class="help is-danger">{{$errors->first('title')}}</p>
            @enderror
        </div>
    </div>


    <div class="field">
        <label class="label">Description of Project</label>
        <div class="control">
            <textarea class="textarea @error('description') is-danger @enderror" placeholder="Project description" id="description" name ="description" v-model="description">{{old('description')}}</textarea>
            @error('description')
            <p class="help is-danger">{{$errors->first('description')}}</p>
            @enderror
        </div>
    </div>


    <div class="field is-grouped">
        <div class="control">
            <button class="button is-link">Submit</button>
        </div>
        <div class="control">
            <button class="button is-link is-light">Cancel</button>
        </div>
    </div>
    </form>
</div>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
        <script src="https://unpkg.com/vue"></script>
        <script src="/js/app.js"></script>

@endsection

