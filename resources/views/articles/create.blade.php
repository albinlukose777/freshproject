@extends ('newSimpleLayout')
@section('bulma')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">
<script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
@endsection

@section('content')
    <form method="POST" action="/articles">
        @csrf
    <div class="field">
        <label class="label" for="title">Title</label>
        <div class="control">
            <input class="input @error('title') is-danger @enderror"
                   type="text"
                   placeholder="Article title"
                   name ="title"
                   id="title"
                   value="{{old('title')}}"
            >
            @error('title')
            <p class="help is-danger">{{$errors->first('title')}}</p>
            @enderror
        </div>
    </div>

    <div class="field">
        <label class="label">Excerpt</label>
        <div class="control">
            <input class="input @error('excerpt') is-danger @enderror" type="text" placeholder="Article Excerpt" name ="excerpt" id="excerpt"value="{{old('excerpt')}}">
            @error('excerpt')
            <p class="help is-danger">{{$errors->first('excerpt')}}</p>
            @enderror
        </div>
    </div>


    <div class="field">
        <label class="label">Body of article</label>
        <div class="control">
            <textarea class="textarea @error('body') is-danger @enderror" placeholder="Article description" id="body" name ="body">{{old('body')}}</textarea>
            @error('body')
            <p class="help is-danger">{{$errors->first('body')}}</p>
            @enderror
        </div>
    </div>

    <div  class="field">
        <label class="label">Tag Picker</label>
        <div class="select is-multiple">
            <select  multiple name ="tags[]">
                @foreach ($tags as $tag)
            <option value="{{$tag->id}}">{{$tag->name}}</option>
                    
                @endforeach
            </select>
            @error('body')
            <p class="help is-danger">{{$errors->first('body')}}</p>
            @enderror
        </div>
    </div>

    <div class="field is-grouped">
        <div class="control">
            <button class="button is-link">Submit</button>
        </div>
        <div class="control">
            <button class="button is-link is-light">Cancel</button>
        </div>
    </div>
    </form>
@endsection

