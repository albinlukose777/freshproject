@extends ('newSimpleLayout')
@section  ('content')
    <div id="wrapper">
        <a href="articles/create" class="float">
 <div id ="inner-div">           
<i class="fa fa-plus my-float" ></i>
</div></a>
        <div id="page" class="container">
            @forelse ($articles as $article)
                <div id="content">
                    <div class="title">
                        <h2>
                            <a href="{{$article->path()}}">  
                            {{$article->title}}</a></h2>
                        <span class="byline">{{$article->excerpt}}</span> </div>
                    <p><img src="images/article.jpg" alt="" class="image image-full" /> </p>

                    <p>{{$article->body}}</p>
                </div>
                @empty
                <p>no relevant articles</p>
            @endforelse
        </div>
    </div>
@endsection
