 @extends ('newSimpleLayout')
@section('bulma')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">
    <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
@endsection

@section('content')
    <form method="POST" action="/articles/{{$article->id}}">
        @csrf
        @method('PUT');

        <div id="wrapper">
            <div id="page" class="container">
                <div id="content">
                    <div class="title">
                        <h2>Update Article</h2>
                </div>
            </div>
        </div>
        <div class="field">
            <label class="label" for="title">Title</label>
            <div class="control">
                <input class="input" type="text" value="{{$article->title}}" name ="title" id="title">
            </div>
        </div>

        <div class="field">
            <label class="label">Excerpt</label>
            <div class="control">
                <input class="input" type="text" value="{{$article->excerpt}}" name ="excerpt" id="excerpt">
            </div>
        </div>


        <div class="field">
            <label class="label">Body of article</label>
            <div class="control">
                <textarea class="textarea"  id="body" name ="body">{{$article->body}}</textarea>
            </div>
        </div>


        <div class="field is-grouped">
            <div class="control">
                <button class="button is-link">Submit</button>
            </div>
            <div class="control">
                <button class="button is-link is-light">Cancel</button>
            </div>
        </div>
    </form>
@endsection

