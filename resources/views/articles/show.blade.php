@extends ('newSimpleLayout')
@section  ('content')
    <div id="wrapper">

        <div id="page" class="container">                           
            <div id="content">
                <div class="title">
                    <h2>{{$article->title}}</h2>
                    <span class="byline">{{$article->excerpt}}</span> </div>                
                <p>{{$article->body}}</p>
                <p>
                    @foreach ($article->tags as $tag)
                <a href="/articles?tag={{$tag->name}}">{{$tag->name}}&nbsp;&nbsp;</a>
                    @endforeach
                </p>
                <i class="fa fa-pencil-square-o" ></i>
            </div>

        </div>
    </div>
@endsection
