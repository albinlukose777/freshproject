<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Article extends Model
{
    //
    protected $guarded=[];// issue with mass assignment
    public function path()
    {
        return route('articles.show',$this);
    }
    public function user()
    {
        //
        return $this->belongsTo(User::class);//select * from users where articleid="";
    }
    public function tags()
    {
        //
        return $this->belongsToMany(Tag::class);//select * from users where articleid="";
    }
    //project->user;
}

//an article has many tags
//a tag can be map with many articles