<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    //
    public function user()
    {
        //
        return $this->belongsTo(User::class);//select * from users where project_id="";
    }
}
//hasone
//hasmany
//belongsto
//belongstomany     
