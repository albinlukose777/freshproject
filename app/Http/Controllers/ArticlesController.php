<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\Tag;

class ArticlesController extends Controller
{
    //
    public function show(Article $article)
    {
        //show a specific article
        //dd($articleid);
        //dd(1);
       // $article = Article::find($articleid);
        return view('articles.show', ['article' => $article]);
    }

    public function index()
    {
        //list of articles
        //dd($articleid);
        //dd(1);
        if(request('tag'))
        {
           $articles=Tag::where('name',request('tag'))->firstOrFail()->articles;
        }else
        $articles = Article::latest()->get();

        return view('articles.index', ['articles' => $articles]);

    }

    public function create()
    {
     //shows a form to create a new article
        return view('articles.create',['tags'=>Tag::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        //submission handle.persist the crearte form
       /* dump(request()->all());
        dump(request('title'));
        dump(request('excerpt'));
        dump(request('body'));*/

       //lp validation control begins
       $this->getValidate();
       $article=new Article(request(['title','excerpt','body']));//temp
       $article->user_id=1;//temp
       $article->save();//temp
       $article->tags()->attach(request('tags'));

       // Article::create($this->getValidate());
        return redirect(route('articles.index'));

      /* $article= new Article();
       $article->title=request('title');
        $article->excerpt=request('excerpt');
        $article->body=request('body');
        $article->save();*/



    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */


    public function edit(Article $article )
    {
        //show a form to edit the existing resource.
        //$article = Article::find($articleid);
        //dump($article->all());
        //dump($article->title);
        return view('articles.edit', compact('article'));
        /**
         * Update the specified resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @param \App\Project $project
         * @return \Illuminate\Http\Response
         */
    }
    public function update(Request $request, Article $article )
    {
        //persist the edited article
        //$article = Article::find($articleid);
/*        $article->title=request('title');
        $article->excerpt=request('excerpt');
        $article->body=request('body');
        $article->save();*/


        $article->update($this->getValidate());
        return redirect($article->path());//lp url stuff add variable in url

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        //delete the article
    }

    /**
     * @return mixed
     */
    public function getValidate()
    {
        return request()->validate([
            'title' => 'required|max:255',
            'excerpt' => 'required',
            'body' => 'required',
            'tags' =>'exists:tags,id'
        ]);
    }
}
